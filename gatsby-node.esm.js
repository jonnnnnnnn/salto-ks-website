/* eslint no-undef: 0 */

/*
 * ESLint throws errors for 
 * 'exports' and 'process' being undefined
 */

const path = require(`path`);

const connectUrl = process.env.CONNECT_API_URL || `https://connect.my-clay.com/v1.1/swagger`;
const coreUrl = process.env.CORE_API_URL || `https://hardware.my-clay.com/swagger/v1.2/swagger.json`;

import getJson from './helpers/swaggerDocumentation';
import getMD from './helpers/getMarkdown';

const untranslateable = [
	`page_endpoint`,
	`page_answer`,
	`page_support`,
	`dev_navigation`,
	`single`,
	`single_story`
];

const doNotRender = [
	`lead_database_item`,
	`single_dealer`
];

const languages = [
	`de`,
	`nl`,
	`fr`,
	`no`,
	`sv`,
	`cs`,
	`da`,
	`es`,
	`fi`,
	`pl`,
	`pt`,
	`it`
];

// Fixes the dotenv error where it can't find 'fs'
exports.onCreateWebpackConfig = ({ actions }) => {
	actions.setWebpackConfig({
		node: {
			fs: `empty`
		}
	});
};

const getComponent = (id, lang) => {
	let component = englishPagesMap[id];
	console.log(`checking ${id} with ${component}`)
	return component && component === `page`;
};

const englishPagesMap = {};
const englishLinksMap = {};
let services = {};

exports.createPages = async ({ graphql, actions }) => {
	const { createPage } = actions;

	await getSwaggerData();

	return new Promise((resolve, reject) => {

		const storyblokEntry = path.resolve(`src/templates/storyblok-entry.js`);

		resolve(
			graphql(
				`{
                    allStoryblokEntry {
                        edges {
                        	node {
								id
								name
								created_at
								uuid
								field_component
								slug
								full_slug
								content
								is_startpage
								parent_id
								group_id
								lang
								first_published_at
								internalId
							}
						}
					}
                }`
			).then(result => {
				if (result.errors) {
					console.log(result.errors);
					reject(result.errors);
				}

				const entries = result.data.allStoryblokEntry.edges;

				const pages = entries.filter((entry) => entry.node.field_component != `navigation`);
				const navItems = entries.filter(entry => entry.node.field_component === `navigation`);
				const englishNav = navItems.find(entry => entry.node.lang === `default`);
				const englishNavItems = JSON.parse(englishNav.node.content).main_navigation;

				// Create the map with all the english links { id: slug }
				englishNavItems.forEach(navItem => {
					if (navItem.link) {
						englishLinksMap[navItem.link.id] = navItem.link.cached_url;
					}
					if (navItem.nav_child && navItem.nav_child.length) {
						navItem.nav_child.forEach(child => {
							if (child.link) {
								englishLinksMap[child.link.id] = child.link.cached_url;
							}
						});
					}
				});

				// Create the map with all english pages { id: [finalised_stranslations]}
				pages.forEach(entry => {
					if (entry.node.lang === `default` && !untranslateable.includes(entry.node.field_component)) {
						englishPagesMap[entry.node.uuid] = entry.node.field_component;
					}
				});

				const connectNav = entries.find(entry => (
					entry.node.field_component === `dev_navigation`
					&& entry.node.slug === `connect-navigation`
					&& entry.node.lang === `default`
				));

				const coreNav = entries.find(entry => (
					entry.node.field_component === `dev_navigation`
					&& entry.node.slug === `core-navigation`
					&& entry.node.lang === `default`
				));

				pages.forEach( async(entry) => {
					const content = JSON.parse(entry.node.content);
					const lang = entry.node.lang;
					const component = entry.node.field_component;
					

					// Get the mobile SDK documentation for the mobile sdk page
					if(entry.node.internalId === 38159755 && entry.node.lang === `default`){
						let documentationiOs = await getMD(content.ios_readme);
						let documentationAndroid = await getMD(content.android_readme);

						content.documentationiOs = documentationiOs;
						content.documentationAndroid = documentationAndroid;
					}


					if (doNotRender.includes(component)) {
						return;
					}

					if (lang !== `default`) {

						// If a page isn't translateable (support, developers, stories, blog posts)
						if (untranslateable.includes(component)) {
							return;
						}

						// If a page is not completely translated
						if (content.finalised_translations && !content.finalised_translations.includes(lang)) {
							if(process.env.CONTEXT === `production`){
								return;
							}
						}
					}


					// Endpoint pages menu and endpoint data mapping
					if (component === `page_endpoint`) {

						content.navigation = entry.node.full_slug.includes(`developers/connect`)
							? connectNav
							: coreNav
						;


						if (content.endpoint) {
							content.endpointData = getEndpointData(content.endpoint);
						}

					}

					const oldSlug = entry.node.full_slug;

					let pagePath = oldSlug.startsWith(`home`) ? oldSlug.replace(`home`, ``) : oldSlug;
					if (pagePath.charAt(pagePath.length - 1) == `-`) {
						pagePath = pagePath.substr(0, pagePath.length - 1);
					}
					if(pagePath.endsWith(`/`)){
						pagePath = pagePath.substr(0, pagePath.length - 1);
					}
					const id = entry.node.group_id;
					const siblings = entries.filter(entry => entry.node.group_id === id);
					const navigation = navItems.find(item => item.node.lang === lang);
					const navContent = typeof navigation.node.content === `string` ? JSON.parse(navigation.node.content) : navigation.node.content;
					const isTranslated = content.finalised_translations && content.finalised_translations.includes(lang);

					const parsedSiblings = [];

					// Assign siblings in other languages
					if (!untranslateable.includes(component)) {
						siblings.map(sibling => {
							if (content.finalised_translations && content.finalised_translations.includes(sibling.node.lang)) {
								const lang = sibling.node.lang === `default` ? `en` : sibling.node.lang;
								const oldSlug = sibling.node.full_slug;
								const newSlug = oldSlug.startsWith(`home`) ? oldSlug.replace(`home`, ``) : oldSlug;
								parsedSiblings.push({
									lang,
									full_slug: newSlug
								});

							}
						});
					}

					entry.node.content = JSON.stringify(content);

					// Create pages in untranslated languages, with english content
					// Only for main pages (ie. excluding developer portal, support answer, blog posts)

					let isDeveloperPage = entry.node.uuid === `aff78cdf-ee8b-4fd9-a0b2-3252667208ce` || entry.node.uuid === `05d0c7f0-40df-41f5-b3b9-510a52de0647`;
					if (
						(!untranslateable.includes(component) || isDeveloperPage)
						&& lang === `default`
					) {
						languages.forEach(lang => {
							if(isDeveloperPage || (content.finalised_translations && !content.finalised_translations.includes(lang))){
								if(pagePath.endsWith(`/`)){
									pagePath = pagePath.substr(0, pagePath.length - 1);
								}
								createPage({
									path: `/${lang}/${pagePath}`,
									component: storyblokEntry,
									context: {
										story: entry.node,
										metadata: {
											published: entry.node.first_published_at,
											updated: entry.node.published_at,
											title: entry.node.name
										},
										siblings: parsedSiblings,
										navigation: {
											properPage: !untranslateable.includes(entry.node.field_component),
											lang: lang,
											items: navContent.main_navigation
										}
									}
								});
							}
						})
					}

					// Create the page
					createPage({
						path: `/${pagePath}`,
						component: storyblokEntry,
						context: {
							story: entry.node,
							metadata: {
								published: entry.node.first_published_at,
								updated: entry.node.published_at,
								title: entry.node.name
							},
							siblings: parsedSiblings,
							navigation: {
								properPage: !untranslateable.includes(entry.node.field_component),
								lang: lang,
								items: navContent.main_navigation
							}
						}
					});
				});

			})
		);
	});
};

const getSwaggerData = async () => {

	let connect = await getJson(connectUrl);
	let core = await getJson(coreUrl);

	services.connect = connect;
	services.core = core;
	return true;
};

const getEndpointData = (endpointData) => {
	const endPointName = endpointData.endpoint;

	if (!endPointName) return;

	const method = endpointData.method;
	const service = endpointData.service;

	const swaggerObj = JSON.parse(services[service]);
	let data;

	try {
		console.log(`try ${endPointName}`)
		data = swaggerObj[endPointName][method];
		return data;
	} catch (e) {
		let version = `v{version}`;
		let toReplace = service === `connect` ? `v1.1` : `v1.2`;
		let newEndpointName = endPointName.replace(toReplace, version);
		try {
			console.log(`trying with ${newEndpointName}`);
			data = swaggerObj[newEndpointName][method];
			return data;
		} catch(e){
			console.warn(`Error getting endpoint data after 2nd try: ${e}`);
			console.log(endPointName, method);
			console.log(`------`);
		}
	}
};
