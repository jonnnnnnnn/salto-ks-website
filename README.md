# Salto KS website

The commercial website for Salto KS [https://www.saltoks.com](https://saltoks.com)
[![Netlify Status](https://api.netlify.com/api/v1/badges/fc4cda70-463e-45ac-b32e-0a0719dd95c0/deploy-status)](https://app.netlify.com/sites/saltoks/deploys)


## Setup
The content is hosted on Storyblok CMS,he code is on Gitlab, and it's deployed on Netlify.

**How this works**

On every content update on the CMS or On every commit to the gitlab repo, we trigger a build on Netlify. This is done through a webhook.
On netlify, the build process is run, which gathers all the content and the code, and generates static pages, and deploys them to the CDN.

*Accounts for all these services are on Bitwarden*

## Local Server
### Development
First, install the dependencies with
```
npm install
```

Then, make sure you have the .env file in root directory. You can find it in Bitwarden as a secure note, with the name ".ENV file saltoks.com".

Then run
```
npm start
```
This will create a development build and you can access it on `http://localhost:8000`

On `http://localhost:8000/__graphql` you can access the GraphQL In-browser explorer.

### Production
To create a production build, run
```
gatsby build && gatsby serve
```
This will generate a production build, and serve it on `http://localhost:9000`

