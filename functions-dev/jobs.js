import axios from 'axios';
import timestamp from 'time-stamp';
import mandrill from 'mandrill-api/mandrill';
import StoryblokClient from 'storyblok-js-client';
const HOMERUN_TOKEN = process.env.HOMERUN_API_KEY;
const HOMERUN_LINK = `https://api.homerun.co/v1/jobs`;

exports.handler = async (event, context, callback) => {
    if (event.httpMethod !== `GET`) {
        return { statusCode: 405, body: `Method Not Allowed` };
    }

    const options = {
        status: event.queryStringParameters.status,
    };

    let jobs;
    try {
        jobs = await getJobs(options, HOMERUN_TOKEN);
    } catch (e) {
        console.log(e);
    }

    if (jobs.status === 200) {
        return {
            statusCode: 200,
            body: JSON.stringify(jobs.data.data),
        };
    } else {
        return {
            statusCode: jobs.status,
            body: JSON.stringify(jobs.data),
        };
    }
};

const getJobs = async (options, token) => {
    const axiosOptions = {
        method: `GET`,
        params: {
            ...options,
        },
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: HOMERUN_LINK,
    };

    let result;

    try {
        result = await axios(axiosOptions);
    } catch (e) {
        console.log(e);
    }
    return result;
};
