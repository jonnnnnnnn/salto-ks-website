import axios from 'axios';
import StoryblokClient from 'storyblok-js-client';
const STORYBLOK_KEY = `rTEXKYwPJo5RgIS25Vl9qwtt`;

let blacklist = [
	`lead_database_item`
]

exports.handler = async (event, context, callback) => {

	if (event.httpMethod !== `POST`) {
		return { statusCode: 405, body: `Method Not Allowed` };
	}

	console.log(event.body);
	let body = JSON.parse(event.body);

	let id = body.story_id;

	let storyData;

	try {
		storyData = await getStory(id);
	} catch(e){
		return {
			statusCode: 500,
			body: `Something went wrong`
		}
	}

	if(storyData.error){
		return {
			statusCode: story.error,
			body: story.body
		}
	} else {
		if(storyData.data.story.content){
			let component = storyData.data.story.content.component;
			
			if(!blacklist.includes(component)){
				let build;
				try {
					build = await triggerBuild();
				} catch(error){
					return {
						statusCode: 500,
						body: `Something went wrong`
					}
				}

				console.log(build);

				if(build.error){
					return {
						statusCode: build.error,
						body: build.body
					}
				}
			}	
		}
	}

	return {
		statusCode: 200,
		body: `Finished with no errors`
	}

};

const getStory = async (id) => {

	return new Promise((resolve, reject) => {
		const Storyblok = new StoryblokClient({
			accessToken: STORYBLOK_KEY
		});

		console.log(id);
		Storyblok.get(`cdn/stories/${id}`, {}).then(response => {
			resolve(response);
		}).catch(error => {
			// console.log(error);
			resolve({
				error: error.response.status,
				body: error.response.statusText
			});
		})
	});

}

const triggerBuild = async () => {
	return new Promise((resolve, reject) => {
		axios.post(`https://api.netlify.com/build_hooks/5f3cf52f4cd58a06304ccfad`).then(response => {
			resolve(response);
		}).catch(error => {
			resolve({
				error: 500,
				body: `Something went wrong`
			})
		})
	});
}