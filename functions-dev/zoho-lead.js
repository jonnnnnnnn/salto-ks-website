import axios from 'axios';
import timestamp from 'time-stamp';
import mandrill from 'mandrill-api/mandrill';
import StoryblokClient from 'storyblok-js-client';
import { GET_A_QUOTE_TEMPLATE, GET_A_QUOTE_SHORT_TEMPLATE, errorTemplate, salesTemplate } from './mail-templates.js';
const STORYBLOK_KEY = `iasW4rHM8G5PdLeGxO5fzQtt-59101-faPx_xx6Wzvv1t9DrDnB`;
const MAN = new mandrill.Mandrill(`G4-Me0NnBBR7cwhVG1oGnA`);

const querystring = require(`querystring`);

exports.handler = async (event, context, callback) => {

	if (event.httpMethod !== `POST`) {
		return { statusCode: 405, body: `Method Not Allowed` };
	}

	let data = (JSON.parse(event.body));
	const { info, type, sendToZoho } = data;


	if(!sendToZoho) {
		await sendEmailToUser(info, type);
		await addToDB(data, true);
		return {
			statusCode: 200,
			body: `added`
		};
	}
	const token = await getToken();

	if (token.error) {
		return {
			statusCode: 401,
			body: `There was a problem authenticating to the server;`
		};
	}

	const pardot = await sendToPardot(info);

	if (pardot.success) {

		try {
			await sendEmailToUser(info);
			await sendEmailToSales(info);
			await addToDB(data, true);
			return {
				statusCode: 200,
				body: `Added successfully`
			};
		} catch (e) {
			return handleError(pardot, data, e);
		}

	} else {
		return handleError(account, data);
	}
};

const addToDB = 	async (data, isSuccess) => {

	const { info, type } = data;

	console.log('adding to db');
	const slug = `${slugify(info.full_name)}-${timestamp(`YYYYMMDDss`)}`;
	const storyData = {
		"story": {
			"name": info.full_name,
			"slug": slug.split(` `).join(`-`),
			"parent_id": 9646478,
			"content": {
				"component": `lead_database_item`,
				"lead_name": info.full_name,
				"email": info.email,
				"phone": info.phone,
				"date": timestamp(`YYYY/MM/DD HH:mm:ss`),
				"status": isSuccess ? `Successful` : `Failed`,
				"source": type === `zoho-quote` ? `quote` : type,
				"info": JSON.stringify(info),
			}
		},
		"publish": 1
	};

	const options = {
		method: `POST`,
		headers: {
			'Authorization': STORYBLOK_KEY,
			'content-type': `application/json`
		},
		url: `https://mapi.storyblok.com/v1/spaces/72378/stories`,
		data: storyData
	};
	const result = await axios(options);

	if(result.status === 201){
		console.log(`added to db`)
	} else {
		const message = {
			"html": `
				<span>Error adding lead into the database</span>
				<br/>
				<span>User info:</span>
				<pre>${JSON.stringify(data.info)}</pre>
				<br/>
				<span>Error:</span>
				<pre>${JSON.stringify(result)}</pre>
			`,
			"subject": `Error adding lead to database`,
			"from_email": `noreply@saltoks.com`,
			"from_name": `Salto KS`,
			"headers": {
				"Reply-To": `jon@my-clay`
			},
			"to": [
				{
					"email": `jon+error@my-clay.com`,
					"type": `to`
				}
			]
		};
		sendEmail(message);
	}
	return true;
};

const handleError = (error, data, e = ``) => {
	const { userInfo } = data;
	const message = {
		"html": `${errorTemplate(error, userInfo, e)}`,
		"subject": `Something went wrong with the Zoho Lead function!`,
		"from_email": `noreply@saltoks.com`,
		"from_name": `Salto KS`,
		"headers": {
			"Reply-To": `jon@my-clay`
		},
		"to": [
			{
				"email": `jon+error@my-clay.com`,
				"type": `to`
			}
		]
	};

	sendEmail(message);
	addToDB(data, false);
	
	return {
		statusCode: 500,
		body: JSON.stringify(error)
	};
};

const sendEmailToUser = async (data, type) => {
	const message = {
		"html": (type && type === `short-quote`) ? GET_A_QUOTE_SHORT_TEMPLATE : GET_A_QUOTE_TEMPLATE,
		"subject": `Thank you for contacting SALTO KS`,
		"from_email": `noreply@saltoks.com`,
		"from_name": `SALTO KS`,
		"headers": {
			"Reply-To": `sales@my-clay`
		},
		"to": [
			{
				"email": data.email,
				"type": `to`
			},
			{
				"email": `jon@my-clay.com`,
				"type": `bcc`
			}
		]
	};

	const email = await sendEmail(message);
	return email;

};

const sendEmailToSales = async data => {
	const message = {
		"html": `${salesTemplate(data)}`,
		"subject": `Website Lead | SALTO KS Contact form`,
		"from_email": `noreply@saltoks.com`,
		"from_name": `SALTO KS`,
		"headers": {
			"Reply-To": `sales@my-clay`
		},
		"to": [
			{
				"email": `jon@my-clay.com`,
			},
			{
				"email": `sales@my-clay.com`,
			}
		]
	};

	return await sendEmail(message);
};

const sendEmail = async message => {
	try {
		MAN.messages.send(
			{ "message": message },
			res => {
				console.log(res);
				return true;
			},
			error => {
				console.log(`An error occurred with Mandrill: ${error.name} - ${error.message}`);
				return false;
			}
		);
	} catch (e) {
		console.log(`Something went wrong trying to send email: ${e}`);
		return false;
	}
};

const sendPotential = async (data, type, token) => {
	const dealName = type === `zoho-quote`
		? `${data.company_name} + ${data.business_type} + SALTO KS quote request`
		: `${data.company_name} + ${data.business_type} + pot. KS CONNECT partner`
	;

	const description = type === `zoho-quote`
		? `Get A Quote Created At`
		: `Initially created through KS Connect form At`
	;
	let existingPotential = await searchRecord(`deals`, `Deal_Name`, dealName, token);
	
	let update = false;
	let existingID;

	if(existingPotential.status === 200){
		console.log(`Potential exists`);
		update = true;
		existingID = existingPotential.data.data[0].id;
	}

	const toSendData = {
		existingID: existingID,
		recordData: {
			"data": [
				{
					"Deal_Name": dealName,
					"Account_Name": {
						"name": data.account.name,
						"id": data.account.id
					},
					"Building_Type": data.business_type,
					"Country_for_TM": data.country,
					"Created_Time": timestamp(`YYYY/MM/DD HH:mm:ss`),
					"Description": `${description} ${timestamp(`YYYY/MM/DD HH:mm:ss`)} + Business Needs: ${data.extra_info} + Locks: ${data.nr_locks} + Users: ${data.nr_guests} + Global Presence: ${data.global_presence} + Number of Locations: ${data.nr_locations} + Lead Name: ${data.full_name} + Reason of Interest: ${data.reason_of_interest}`,
					"Quantity_Product_1": data.nr_locks,
					"Quantity_Product_2": data.nr_guests,
					"Stage": `Qualification`,
					"Type_of_scenario": `SALTO KS`,
					"Kind_of_work": `Renovation / Modification`,
					"Territory": data.country,
					"Lead_Source": `Saltoks.com (salto ks website)`,
					"State": data.state || ``
				}
			]
		}
		
	};

	const newPotential = await registerRecord(`deals`, toSendData, token, update);

	if (newPotential.data.data[0].code === `SUCCESS` || newPotential.data.data[0].code === `DUPLICATE_DATA`) {

		console.log(`New potential successfully added!`);
		return {
			success: 1,
			id: newPotential.data.data[0].details.id,
			data: toSendData.potentialData
		};
	} else {
		return {
			success: 0,
			message: `Something went wrong`,
			data: newPotential.data.data[0]
		};
	}

};

const sendContact = async (data, type, token) => {

	const toSendData = {
		recordData: {
			"data": [
				{
					"Last_Name": data.full_name,
					"Full_Name": data.full_name,
					"Email": data.email,
					"Phone": data.phone,
					"VERTICAL": data.business_type,
					"Company_name": data.company_name,
					"Created_Time": timestamp(`YYYY/MM/DD HH:mm:ss`),
					"Country_for_TM": data.country,
					"Mailing_Country": data.country,
					"Territories": data.country,
					"Mailing_State": data.state || ``,
					"Mailing_City": data.city,
					"Lead_Source": `Other`,
					"Account_Name": {
						"name": data.account.name,
						"id": data.account.id
					},
					"Description": `Locks Number: ${data.nr_locks} // Users Number: ${data.nr_guests} // Company Site: ${data.website} // Extra Info: ${data.extra_info} // Lead Name: ${data.full_name} //  Reason of Interest: ${data.reason_of_interest}`,
					"Data_Source": `Webform`
				}
			]
		}
		

	};


	const newContact = await registerRecord(`contacts`, toSendData, token);

	if (newContact.data.data[0].code === `SUCCESS` || newContact.data.data[0].code === `DUPLICATE_DATA`) {

		console.log(`New contact successfully added!`);

		return {
			success: 1,
			id: newContact.data.data[0].details.id,
			data: toSendData.recordData.data[0]
		};
	} else {
		return {
			success: 0,
			message: `Something went wrong`,
			data: toSendData.recordData	.data.data[0]
		};
	}

};

const sendAccount = async (info, type, token) => {

	let existingAcc = await searchRecord(`accounts`, `EMail_Address`, data.email, token);
	
	let update = false;
	let existingID;

	if(existingAcc.status === 200){
		console.log(`account exists`);
		update = true;
		existingID = existingAcc.data.data[0].id;
	}

	const data = {
		email: info.email || ``,
		country: info.country || ``,
		state: info.state || ``,
		city: info.city || ``,
		first_name: info.first_name || ``,
		last_name: info.last_name || ``,
		telephone_prefix: info.prefix || ``,
		phone: info.telephone || ``,
		contact_method:info.contact_method || ``,
		profile: info.profile || ``,
		message: info.message || ``,
		building_type: info.building_type || ``,
		company: info.company || ``,
		job: info.job || ``,
		website: info.website || ``
	};

	let encodedParams = queryString.encode(data);

	let url = `https://pardot.saltosystems.com/l/869141/2020-10-30/4gqjm4?${encodedParams}`;
	const options = {
		method: `POST`,
		url: `${url}${update ? `/${data.existingID}` : ``}`,
	};

	return await axios(options);

};

const searchRecord = async (type, criteriaName, criteriaValue, token) => {
	console.log(`searching ${type} with ${criteriaName} ${criteriaValue}`)
	const options = {
		method: `GET`,
		headers: {
			'Authorization': `Zoho-oauthtoken ${token}`,
			'content-type': `application/x-www-form-urlencoded`
		},
		url: `https://www.zohoapis.com/crm/v2/${type}/search?criteria=((${criteriaName}:equals:${encodeURIComponent(criteriaValue)}))`,
	};
	return await axios(options);
}

const registerRecord = async (type, data, token, update) => {
	if(update){
		console.log(`Updating ${type} with id ${data.existingID}`)
	} else {
		console.log(`Creating ${type}`);
	}
	let url = `https://www.zohoapis.com/crm/v2/${type}`;
	const options = {
		method: update ? `PUT` : `POST`,
		headers: {
			'Authorization': `Zoho-oauthtoken ${token}`,
			'content-type': `application/x-www-form-urlencoded`
		},
		url: `${url}${update ? `/${data.existingID}` : ``}`,
		data: data.recordData
	};
	return await axios(options);
};


const getToken = async () => {
	const body = {
		refresh_token: `1000.fc0df0e466686df4cfbb66d03c01b2b9.0a1039d4c97c761289d3b2cb3d11aa52`,
		client_id: `1000.FXNK9WZZS5ISHMGVGKYYQ3M7RMUC9H`,
		client_secret: `fba23955ff48f44d87d2821a7ecd81007b991337b8`,
		grant_type: `refresh_token`
	};
	const url = `https://accounts.zoho.com/oauth/v2/token`;

	const response = await axios.post(`${url}?refresh_token=${body.refresh_token}&client_id=${body.client_id}&client_secret=${body.client_secret}&grant_type=${body.grant_type}`);
	return response.data;
};


const slugify = (string) => {
	const a = `àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;`;
	const b = `aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------`;
	const p = new RegExp(a.split(``).join(`|`), `g`);

	return string.toString().toLowerCase()
		.replace(/\s+/g, `-`) 						// Replace spaces with -
		.replace(p, c => b.charAt(a.indexOf(c)))	// Replace special characters
		.replace(/&/g, `-and-`) 					// Replace & with 'and'
		.replace(/[^\w\-]+/g, ``) 					// Remove all non-word characters
		.replace(/\-\-+/g, `-`) 					// Replace multiple - with single -
		.replace(/^-+/, ``) 						// Trim - from start of text
		.replace(/-+$/, ``); 						// Trim - from end of text
};
