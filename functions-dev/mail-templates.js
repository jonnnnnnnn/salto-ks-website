const GET_A_QUOTE_TEMPLATE = `
	<html xmlns="http://www.w3.org/1999/xhtml"><head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <title>Thank you for reaching out</title> <meta name="viewport" content="width=device-width, initial-scale=1.0"> </head> <body style="margin: 0; padding: 0;"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;"> <tbody><tr> <td style="background-color: #F7F7F7;" align="center"> <div style="color: #0088CE; font-family: Arial, sans-serif; font-size: 38px; padding: 100px 0px 0px 0px; width:300px; line-height: 50px;"> Thanks for reaching out! </div></td></tr><tr> <td style="background-color: #F7F7F7;padding-top: 50px;" align="center"> <img src="https://img2.storyblok.com/f/72378/275x183/83f9469778/saltoks-tag.png" alt="Salto tag" style="display: block;"> </td></tr><tr> <td align="center"> <div style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 20px 0px 0px 0px; text-align: left; width: 460px; margin-top: 21px; line-height: 30px;"> We have received your information and will be contacting you within two business days. </div></td></tr><tr> <td align="center" style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 0px 0px 0px 0px;"> <div style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 0px 0px 20px 0px; text-align: left; width: 460px; margin-top: 21px; line-height: 30px;"> In the meantime, take a look at our <a href="https://saltoks.com/blog" target="_blank" style="color: #0088CE; text-decoration:none;" title="SALTO KS blog">blog</a> for product updates, industry insights and much more or get social with us on <a title="SALTO KS Instagram" href="https://www.instagram.com/saltoks/" target="_blank" style="color: #0088CE; text-decoration:none;">Instagram</a> and <a title="SALTO KS LinkedIn" href="https://www.linkedin.com/company/5130587/" target="_blank" style="color: #0088CE; text-decoration:none;">LinkedIn</a>.</div></td></tr><tr> <td align="center" style="color: #0D1333; font-family: Arial, sans-serif; font-size: 14px; margin-top:30px;"> <div style="width: 460px; text-align: left;line-height: 30px;">Have a great day!</div><div style="width: 460px; text-align: left;line-height: 30px;">The team behind SALTO KS </div></td></tr><tr> <td align="center"> <div style="width: 460px; text-align: left;"> <img src="https://img2.storyblok.com/f/72378/184x56/a01da24387/logo-color.png" alt="Salto logo" style="display: block; padding: 50px 0px 50px 0px;"> </div></td></tr></tbody></table></body></html>
`;

const GET_A_QUOTE_SHORT_TEMPLATE = `
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>Thank you for reaching out</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		</head>
		<body style="margin: 0; padding: 0;">
			<table
				align="center"
				border="0"
				cellpadding="0"
				cellspacing="0"
				width="100%"
				style="border-collapse: collapse;"
			>
				<tbody>
					<tr>
						<td style="background-color: #F7F7F7;" align="center">
							<div
								style="color: #0088CE; font-family: Arial, sans-serif; font-size: 38px; padding: 100px 0px 0px 0px; width:300px; line-height: 50px;"
							>
								Thanks for reaching out!
							</div>
						</td>
					</tr>
					<tr>
						<td
							style="background-color: #F7F7F7;padding-top: 50px;"
							align="center"
						>
							<img
								src="https://img2.storyblok.com/f/72378/275x183/83f9469778/saltoks-tag.png"
								alt="Salto tag"
								style="display: block;"
							/>
						</td>
					</tr>
					<tr>
						<td align="center">
							<div
								style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 20px 0px 0px 0px; text-align: left; width: 460px; margin-top: 21px; line-height: 30px;"
							>
								We have received your information and will be
								contacting you within two business days.
							</div>
						</td>
					</tr>
					<tr>
						<td
							align="center"
							style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 0px 0px 0px 0px;"
						>
							<div
								style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; text-align: left; width: 460px; margin-top: 21px; line-height: 30px;"
							>
								Our team will be able to contact you in a faster
								manner when you share more information.
								<a
									href="https://saltoks.com/get-in-touch"
									target="_blank"
									style="color: #0088CE; text-decoration:none;"
									title="Share more information"
								>
									Share more information</a
								>
							</div>
						</td>
					</tr>
					<tr>
						<td
							align="center"
							style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 0px 0px 20px 0px;"
						>
							<div
								style="color: #1E1E1E; font-family: Arial, sans-serif; font-size: 14px; padding: 0px 0px 20px 0px; text-align: left; width: 460px; margin-top: 21px; line-height: 30px;"
							>
								In the meantime, take a look at our
								<a
									href="https://saltoks.com/blog"
									target="_blank"
									style="color: #0088CE; text-decoration:none;"
									title="SALTO KS blog"
									>blog</a
								>
								for product updates, industry insights and much more
								or get social with us on
								<a
									title="SALTO KS Instagram"
									href="https://www.instagram.com/saltoks/"
									target="_blank"
									style="color: #0088CE; text-decoration:none;"
									>Instagram</a
								>
								and
								<a
									title="SALTO KS LinkedIn"
									href="https://www.linkedin.com/company/5130587/"
									target="_blank"
									style="color: #0088CE; text-decoration:none;"
									>LinkedIn</a
								>.
							</div>
						</td>
					</tr>
					<tr>
						<td
							align="center"
							style="color: #0D1333; font-family: Arial, sans-serif; font-size: 14px; margin-top:30px;"
						>
							<div
								style="width: 460px; text-align: left;line-height: 30px;"
							>
								Have a great day!
							</div>
							<div
								style="width: 460px; text-align: left;line-height: 30px;"
							>
								The team behind SALTO KS
							</div>
						</td>
					</tr>
					<tr>
						<td align="center">
							<div style="width: 460px; text-align: left;">
								<img
									src="https://img2.storyblok.com/f/72378/184x56/a01da24387/logo-color.png"
									alt="Salto logo"
									style="display: block; padding: 50px 0px 50px 0px;"
								/>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</body>
	</html>
`
;

const errorTemplate = (error, info, e) => `
	<span>
		There was an error in the Zoho Lead function!
		The error is:
		<pre>
			${JSON.stringify(error)}
		</pre>

		The user info is:
		<pre>
			${JSON.stringify(info)}
		</pre>

		extra info:
		<pre>
			${JSON.stringify(e)}
		</pre>
	</span>
`;

const salesTemplate = info => `
	<p>
		Hi,
		<br/>
		<br/>
		
		We received a lead from the website. The visitor's informations is below. Please follow up by email within 2 business days.<br/>
		
		<br/>

		Company Website: ${info.website || `No info`} <br/>
		Name: ${info.first_name}  ${info.last_name}<br/>
		Company: ${info.company || `No info`}<br/>
		Job: ${info.job || `No info`}<br/>
		Phone: ${info.prefix || ``}${info.telephone || `No info`}<br/>
		Email:  ${info.email}<br/>
		Profile: ${info.profile || `No info`}<br/>
		Building type:  ${info.building_type || `No info`}<br/>
		Message:  ${info.message || `No info`}<br/>

		Country: ${info.country}<br/>
		State: ${info.state || `No info`}<br/>
		City:  ${info.city || `No info`}<br/>
		<br/>
		Prefered method of contact: ${info.contact_method}<br/>
		<br/>
		Commercial communications: ${info[`commercial-comms`] ? `Accept` : `Decline`}<br/>
		Data communicated to SALTO Group Companies: ${info[`data-sharing`] ? `Accept` : `Decline`}<br/>
		<br/>
		Thank you!<br/>
		<br/>
		<br/>
		<br/>
	</p>
`;

export {
	GET_A_QUOTE_TEMPLATE,
	GET_A_QUOTE_SHORT_TEMPLATE,
	errorTemplate,
	salesTemplate,
};
