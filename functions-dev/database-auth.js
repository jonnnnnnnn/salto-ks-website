exports.handler = async (event, context, callback) => {
	if (event.httpMethod !== `POST`) {
		return { statusCode: 405, body: `Method Not Allowed` };
	}

	const data = JSON.parse(event.body);
	const password = data.password;

	if(process.env.LEAD_DATABASE_PASSWORD && password === process.env.LEAD_DATABASE_PASSWORD){
		return {
			statusCode: 200,
			body: JSON.stringify({
				access: true
			})
		};
	} else {
		return {
			statusCode: 401,
			body: JSON.stringify({
				access: false
			})
		};
	}
};