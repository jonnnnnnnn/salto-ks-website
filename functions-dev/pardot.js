import axios from 'axios';
import timestamp from 'time-stamp';
import mandrill from 'mandrill-api/mandrill';
import StoryblokClient from 'storyblok-js-client';
import {
    GET_A_QUOTE_TEMPLATE,
    GET_A_QUOTE_SHORT_TEMPLATE,
    errorTemplate,
    salesTemplate,
} from './mail-templates.js';
const STORYBLOK_KEY = process.env.STORYBLOK_MANAGEMENT_TOKEN;
const MAN = new mandrill.Mandrill(process.env.MANDRILL_KEY);

const querystring = require(`querystring`);

exports.handler = async (event, context, callback) => {
    if (event.httpMethod !== `POST`) {
        return { statusCode: 405, body: `Method Not Allowed` };
    }

    let data = JSON.parse(event.body);
    const { info, type, sendToZoho } = data;

    console.log(data);

    if (!sendToZoho) {
        await sendEmailToUser(info, type);
        await addToDB(data, true);
        return {
            statusCode: 200,
            body: `added`,
        };
    }

    const pardot = await sendToPardot(info);

    console.log(pardot);

    if (pardot.status === 200) {
        try {
            info.full_name = `${info.first_name} ${info.last_name}`;

            await sendEmailToUser(info);
            await sendEmailToSales(info);

            await addToDB(data, true);
            return {
                statusCode: 200,
                body: `Added successfully`,
            };
        } catch (e) {
            return handleError(pardot, data, e);
        }
    } else {
        return handleError(pardot, data);
    }
};

const addToDB = async (data, isSuccess) => {
    const { info, type } = data;

    let full_name = `${info.first_name} ${info.last_name}`;

    console.log(`adding to db`);
    const slug = `${slugify(full_name)}-${timestamp(`YYYYMMDDss`)}`;
    const storyData = {
        story: {
            name: full_name,
            slug: slug.split(` `).join(`-`),
            parent_id: 9646478,
            content: {
                component: `lead_database_item`,
                lead_name: full_name,
                email: info.email,
                phone: `${info.prefix}${info.telephone}`,
                date: timestamp(`YYYY/MM/DD HH:mm:ss`),
                status: isSuccess ? `Successful` : `Failed`,
                source: type === `pardot` ? `quote` : type,
                info: JSON.stringify(info),
            },
        },
        publish: 1,
    };

    const options = {
        method: `POST`,
        headers: {
            Authorization: STORYBLOK_KEY,
            'content-type': `application/json`,
        },
        url: `https://mapi.storyblok.com/v1/spaces/72378/stories`,
        data: storyData,
    };
    const result = await axios(options);

    if (result.status === 201) {
        console.log(`added to db`);
    } else {
        const message = {
            html: `
				<span>Error adding lead into the database</span>
				<br/>
				<span>User info:</span>
				<pre>${JSON.stringify(data.info)}</pre>
				<br/>
				<span>Error:</span>
				<pre>${JSON.stringify(result)}</pre>
			`,
            subject: `Error adding lead to database`,
            from_email: `noreply@saltoks.com`,
            from_name: `SALTO KS`,
            headers: {
                'Reply-To': `jon@my-clay`,
            },
            to: [
                {
                    email: `jon+error@my-clay.com`,
                    type: `to`,
                },
            ],
        };
        sendEmail(message);
    }
    return true;
};

const handleError = (error, data, e = ``) => {
    const { userInfo } = data;
    const message = {
        html: `${errorTemplate(error, userInfo, e)}`,
        subject: `Something went wrong with the Zoho Lead function!`,
        from_email: `noreply@saltoks.com`,
        from_name: `SALTO KS`,
        headers: {
            'Reply-To': `jon@my-clay`,
        },
        to: [
            {
                email: `jon+error@my-clay.com`,
                type: `to`,
            },
        ],
    };

    sendEmail(message);
    addToDB(data, false);

    return {
        statusCode: 500,
        body: JSON.stringify(error),
    };
};

const sendEmailToUser = async (data, type) => {
    const message = {
        html:
            type && type === `short-quote`
                ? GET_A_QUOTE_SHORT_TEMPLATE
                : GET_A_QUOTE_TEMPLATE,
        subject: `Thank you for contacting SALTO KS`,
        from_email: `noreply@saltoks.com`,
        from_name: `SALTO KS`,
        headers: {
            'Reply-To': `sales@my-clay`,
        },
        to: [
            {
                email: data.email,
                type: `to`,
            },
            {
                email: `jon@my-clay.com`,
                type: `bcc`,
            },
        ],
    };

    const email = await sendEmail(message);
    return email;
};

const sendEmailToSales = async data => {
    const message = {
        html: `${salesTemplate(data)}`,
        subject: `Website Lead | Contact form`,
        from_email: `noreply@saltoks.com`,
        from_name: `SALTO KS`,
        headers: {
            'Reply-To': `sales@my-clay`,
        },
        to: [
            {
                email: `jon@my-clay.com`,
            },
            {
                email: `sales@my-clay.com`,
            },
        ],
    };

    return await sendEmail(message);
};

const sendEmail = async message => {
    try {
        MAN.messages.send(
            { message: message },
            res => {
                console.log(res);
                return true;
            },
            error => {
                console.log(
                    `An error occurred with Mandrill: ${error.name} - ${error.message}`
                );
                return false;
            }
        );
    } catch (e) {
        console.log(`Something went wrong trying to send email: ${e}`);
        return false;
    }
};

const sendToPardot = async (info, type, token) => {
    console.log(`sending to pardot`);

    const data = {
        email: info.email || ``,
        country: info.country || ``,
        state: info.state || ``,
        city: info.city || ``,
        first_name: info.first_name || ``,
        last_name: info.last_name || ``,
        telephone_prefix: info.prefix || ``,
        phone: info.telephone || ``,
        contact_method: info.contact_method || ``,
        profile: info.profile || ``,
        message: info.message || ``,
        building_type: info.building_type || ``,
        company: info.company || ``,
        job: info.job || ``,
        website: info.website || ``,
    };

    let encodedParams = querystring.encode(data);

    let url = `https://pardot.saltosystems.com/l/869141/2020-10-30/4gqjm4?${encodedParams}`;

    console.log(url);
    const options = {
        method: `POST`,
        url: url,
    };

    return await axios(options);
};

const slugify = string => {
    const a = `àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;`;
    const b = `aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------`;
    const p = new RegExp(a.split(``).join(`|`), `g`);

    return string
        .toString()
        .toLowerCase()
        .replace(/\s+/g, `-`) // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, `-and-`) // Replace & with 'and'
        .replace(/[^\w\-]+/g, ``) // Remove all non-word characters
        .replace(/\-\-+/g, `-`) // Replace multiple - with single -
        .replace(/^-+/, ``) // Trim - from start of text
        .replace(/-+$/, ``); // Trim - from end of text
};
