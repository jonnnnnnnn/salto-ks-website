import mailchimp from '@mailchimp/mailchimp_marketing';

mailchimp.setConfig({
    apiKey: process.env.MAILCHIMP_KEY,
    server: `us3`,
});

const LIST_ID = `20282b0048`;

exports.handler = async (event, context, callback) => {
    if (event.httpMethod !== `POST`) {
        return { statusCode: 405, body: `Method Not Allowed` };
    }
    console.log(event);

    let data = JSON.parse(event.body);
    const { email } = data;

    let response;

    try {
        response = await mailchimp.lists.addListMember(LIST_ID, {
            email_address: email,
            status: `subscribed`,
        });
    } catch (e) {
        let response = JSON.parse(e.response.text);

        if (response.title.toLowerCase().includes(`exists`)) {
            return {
                statusCode: response.status,
                body: `${email} has already been subscribed to the newsletter.`,
            };
        } else {
            return {
                statusCode: response.status,
                body: response.detail,
            };
        }
    }

    if (response.id) {
        return {
            statusCode: 200,
            body: `Thank you for subscribing!`,
        };
    } else {
        return {
            statusCode: 500,
            body: `Something went wrong. Please reload and try again.`,
        };
    }
};
