import axios from 'axios';
import timestamp from 'time-stamp';
import mandrill from 'mandrill-api/mandrill';
import StoryblokClient from 'storyblok-js-client';
const STORYBLOK_KEY = `iasW4rHM8G5PdLeGxO5fzQtt-59101-faPx_xx6Wzvv1t9DrDnB`;

const MAN = new mandrill.Mandrill(`G4-Me0NnBBR7cwhVG1oGnA`);


exports.handler = async (event, context, callback) => {

	if (event.httpMethod !== `POST`) {
		return { statusCode: 405, body: `Method Not Allowed` };
	}
	const SchfData = JSON.parse(event.body);

	if (SchfData.meta.event === `test`) {
		return {
			statusCode: 200,
			body: `Test Successfull!`,
			headers: {
				"content-type" : `text/json`
			}
		};
	}

	if (SchfData.meta.event === `publish` || SchfData.meta.event === `update`) {

		const isPublish = SchfData.meta.event === `publish`;
		let scriptStart = SchfData.data.content.indexOf(`<!-- strchf script -->`);
		let scriptEnd = SchfData.data.content.indexOf(`<!-- End strchf script -->`);

		let script = SchfData.data.content.slice(scriptStart, scriptEnd);
		let cleanContent = SchfData.data.content.slice(0, scriptStart);
		let category;
		if(SchfData.data.category){
			category = SchfData.data.category.data.slug;
		}
		let newPostData = {
			slug: SchfData.data.seo_slug || slugify(SchfData.data.title),
			name: SchfData.data.title || ``,
			current_id: SchfData.data.external_id,
			content: {
				component: `single`,
				ampurl: SchfData.data.amphtml,
				title: SchfData.data.title,
				content_storychief: cleanContent,
				script_storychief: script,
				date: SchfData.data.published_at,
				category: category || ``,
				hero: [
					{
						componentt: `image`,
						image: SchfData.data.custom_fields.find(field => field.key === `q0rl3xbdBP5kgYmOpVBy`).value,
						min_height: `222`
					}
				],
				thumb: [
					{
						component: `image`,
						image: SchfData.data.featured_image.data.sizes.regular,
					}
				]
			}
		};

		let newPost;

		if (isPublish) {
			newPost = await createStory(newPostData);
		} else {
			newPost = await updateStory(newPostData);
		}

		return {
			statusCode: 200,
			body: JSON.stringify({
				id: newPost.id,
				permalink: `https://new.saltoks.com/${newPost.full_slug}`
			}),
			headers: {
				"content-type" : `text/json`
			}
		};
	}

	if (SchfData.meta.event === `delete`) {
		const storyId = SchfData.data.external_id;

		let deleted;
		
		try {
			deleted = await deleteStory(storyId);
		} catch(e){
			console.log(e);
		}
		
		return {
			statusCode: 200,
			body: JSON.stringify({id: storyId}),
			headers: {
				"content-type" : `text/json`
			}
		};

	}
};


const deleteStory = async (id) => {
	const options = {
		method: `DELETE`,
		headers: {
			'Authorization': STORYBLOK_KEY,
			'content-type': `application/json`
		},
		url: `https://mapi.storyblok.com/v1/spaces/72378/stories/${id}`
	};

	let result;

	try {
		result = await axios(options);
	} catch(e){

		// If it's 404, means story doesn't exist in CMS, so we can consider it deleted
		if(e.response.status === 404){
			return true;
		}
	}

	return result.status === 200;
};

const updateStory = async (data) => {
	const storyData = {
		"story": {
			parent_id: 7030070,
			...data
		},
		"force_update": 1
	};

	const options = {
		method: `PUT`,
		headers: {
			'Authorization': STORYBLOK_KEY,
			'content-type': `application/json`
		},
		url: `https://mapi.storyblok.com/v1/spaces/72378/stories/${data.current_id}`,
		data: storyData
	};
	let result;
	try {
		result = await axios(options);
	} catch (e) {
		console.log(e);
	}

	if (result && result.status && result.status == 200) {
		console.log(`Updated story`);
		return result.data.story;
	} else {
		return result.data.story;
	}
};

const createStory = async (data) => {

	const storyData = {
		"story": {
			parent_id: 7030070,
			...data
		},
	};

	const options = {
		method: `POST`,
		headers: {
			'Authorization': STORYBLOK_KEY,
			'content-type': `application/json`
		},
		url: `https://mapi.storyblok.com/v1/spaces/72378/stories`,
		data: storyData
	};
	let result;
	try {
		result = await axios(options);
	} catch (e) {
		console.log(e);
	}

	if (result && result.status && result.status === 201) {
		console.log(`added to db`);
		return result.data.story;
	} else {
		console.log(`error`);
	}

	return true;
};


const slugify = (string) => {
	const a = `àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;`;
	const b = `aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------`;
	const p = new RegExp(a.split(``).join(`|`), `g`);

	return string.toString().toLowerCase()
		.replace(/\s+/g, `-`) 						// Replace spaces with -
		.replace(p, c => b.charAt(a.indexOf(c)))	// Replace special characters
		.replace(/&/g, `-and-`) 					// Replace & with 'and'
		.replace(/[^\w\-]+/g, ``) 					// Remove all non-word characters
		.replace(/\-\-+/g, `-`) 					// Replace multiple - with single -
		.replace(/^-+/, ``) 						// Trim - from start of text
		.replace(/-+$/, ``); 						// Trim - from end of text
};
