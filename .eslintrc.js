module.exports = {
	'env': {
		'browser': true,
		'es6': true,
		"amd": true
	},
	'extends': [
		'eslint:recommended',
		'plugin:react/recommended'
	],
	'globals': {
		'Atomics': 'readonly',
		'SharedArrayBuffer': 'readonly',
		'module': false
	},
	'parserOptions': {
		'ecmaFeatures': {
			'jsx': true,
			"modules": true
		},
		'ecmaVersion': 2018,
		'sourceType': 'module'
	},
	'plugins': [
		'react',
		'react-hooks'
	],
	'rules': {
		"no-unused-vars": [
			"error",
			{
				"varsIgnorePattern": "^h$"
			}
		],
		'indent': [
			'error',
			'tab'
		],
		'quotes': [
			'error',
			'backtick'
		],
		'semi': [
			'error',
			'always'
		],
		'react-hooks/rules-of-hooks': 'error',
    	'react-hooks/exhaustive-deps': 'warn'
	}
};