const axios = require(`axios`);
const $RefParser = require(`json-schema-ref-parser`);

let count = 0;

const delay = (t, v) => {
	return new Promise(function(resolve) { 
		console.log(`Waiting ${t / 1000} seconds before retrying`);
		setTimeout(resolve.bind(null, v), t);
	});
};

const getJson = async (endpoint) => {
	try {
		return await axios.get(endpoint);
	} catch(e){
		if(count++ < 5){
			console.log(`Attempt nr. ${count} to get swagger endpoint: ${endpoint}`);
			// Sometimes the swagger returns a 404 for no reason, so we wait 20 seconds and try again. Max 5 times.
			await delay(20000);
			return getJson(endpoint);
		} else {
			console.log(e);
			process.exit(1);
		}
	}
};

const init = async (endpoint) => {
	console.log(`Getting API documentation from: ${endpoint}`);
	const jsonObject = await getJson(endpoint);

	try { 
		let content = await $RefParser.dereference(jsonObject.data);
		return JSON.stringify(content.paths);
	}
	catch(err) {
		console.error(err);
	}
};

export default init;