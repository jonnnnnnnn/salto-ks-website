const axios = require(`axios`);
const MarkdownIt = require(`markdown-it`);


const parseMd = async (object) => {
	let parser = new MarkdownIt({
		html: true
	});
	let withHttps = object.replace(`http://`, `https://`);
	return parser.render(withHttps);
};

const getMd = async (endpoint) => {
	try {
		return await axios.get(endpoint);
	} catch(e){
		console.log(`Error getting readme file ${e}`);
		process.exit(1);
	}
};

const init = async (endpoint) => {
	console.log(`Getting MARKDOWN file from: ${endpoint}`);
	const markdownObject = await getMd(endpoint);

	try { 
		return parseMd(markdownObject.data);
	}
	catch(err) {
		console.error(err);
	}
};

export default init;