const fs = require('fs');

const writeFile = (file, content) => {
	return new Promise((resolve, reject) => {
		fs.writeFile(`./${file}.json`, content, `utf8`, function (err) {
			if (err) {
				reject(`Couldn't write files` + err);
			}
			resolve();
		});
	});
};

const readFile = (file) => {
	return new Promise((resolve, reject) => {
		fs.readFile(`./${file}.json`, function (err, data) {
			if (err) {
				reject(`Couldn't write files` + err);
			}
			resolve(data);
		});
	});
};

const getObj = (dealer, key) => {
	return dealer[`wp:postmeta`].find(meta => meta[`wp:meta_key`] === key);
};


const run = async () => {
	const dealersRawData = await readFile(`dealers`);
	const dealersData = JSON.parse(dealersRawData);
	let parsedDealers = [];

	dealersData.channel.item.forEach(dealer => {
		const addressObj = getObj(dealer, `dealer_post_address`);
		const zipObj = getObj(dealer, `dealer_post_zipcode`);
		const cityObj = getObj(dealer, `dealer_post_city`);
		const countryObj = getObj(dealer, `dealer_post_country`);
		const phoneObj = getObj(dealer, `dealer_post_phonenumber`);
		const emailObj = getObj(dealer, `dealer_post_email`);
		const latObj = getObj(dealer, `dealer_post_latitude`);
		const lngObj = getObj(dealer, `dealer_post_longitude`);

		parsedDealers.push({
			title: dealer.title,
			address: addressObj[`wp:meta_value`],
			zip: zipObj[`wp:meta_value`],
			city: cityObj[`wp:meta_value`],
			phone: phoneObj[`wp:meta_value`],
			country: countryObj[`wp:meta_value`],
			email: emailObj[`wp:meta_value`],
			dimensions: {
				lat: latObj[`wp:meta_value`],
				lng: lngObj[`wp:meta_value`]
			}
		});
	});
	await writeFile(`dealersParsed`, JSON.stringify(parsedDealers));
	console.log(`done!`);
};

run();