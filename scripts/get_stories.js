const
	// Story block constants
	StoryblokClient = require(`storyblok-js-client`),
	accessToken = `rTEXKYwPJo5RgIS25Vl9qwtt`,
	spaceId = `72378`, // id of space where to swap content

	Storyblok = new StoryblokClient({
		accessToken
    }),
    
    pdf = require("pdf-creator-node"),
    fs = require('fs')
;

let lastPage = 1;
let result = [];

const SB = {
    getAnswers: (page) => {
        const perPage = 100;
        console.log(`Getting next 100 stories, page: ${page}`);
        return new Promise((resolve, reject) => {
            Storyblok.get(`cdn/stories`, {
                contain_component: `page`,
                filter_query: {
                    component: {
                        in: 'page_answer'
                    }
                },
                page: page,
                perPage: perPage
            })
                .then(async (res) => {
                    console.log(`Successfully fetched page ${page}`);
                    let stories = res.data.stories;
                    result.push(...stories);

                    let total = res.total;
                    lastPage = Math.ceil((res.total / res.perPage));
                    if (page <= lastPage) {
                        page++
                        await SB.getAnswers(page)
                    }

                    resolve(result);
                })
                .catch(e => {
                    console.log(`Something went wrong fetching page ${page}. Error: ${e}`)
                    reject(e)
                });
        });
    }
}


const getAllAnswers = async () => {
    console.log(`Getting all answers:`);
    let result = await SB.getAnswers(1);
    return result;
}

const init = async () => {

    let allAnswers = await getAllAnswers();

    let promises = [];

    allAnswers.forEach(async answer => {
        let content;
        try {   
            if(answer.content.content.type) {
                content = Storyblok.richTextResolver.render(answer.content.content);
            }
        } catch(e){
            console.log(`Could parse content for ${answer.name}: ${e}`)
        }
        const 
            title = answer.name,
            folder = answer.content.category
        ;
        
        if(content){
            promises.push(exportAsPDF(title, folder, content).catch(e => console.log(e)));
        }
    });
    
    console.log(`Init export of all answers...`);

    await Promise.all(promises).then(res => {
        console.log(`Finished exporting all answers:`);
    }).catch(e => {
        e.forEach(error => {
            console.log(`Something went wrong during the export: ${error}`);
        });
    });
    


    
}

init();

const exportAsPDF = async (title, folder, content) => {
    const EXPORT_DIR = `ANSWERS_EXPORT`;
    const PDF_TEMPLATE = 'answer_template.html'
    const pdfOptions = {
        format: "A4",
        orientation: "portrait",
        border: "10mm",
        header: {
            height: "45mm",
            contents: '<div style="text-align: center;"><svg xmlns="http://www.w3.org/2000/svg" width="294" height="90" viewBox="0 0 98 30" class="main-logo"><g fill="none" fill-rule="evenodd"><path fill="#000" d="M22.037.824v1.57h2.265v8.633h1.57V2.395H28.1V.825zM5.565 10.31c-.577.535-1.297.796-2.133.796-1.842 0-2.754-.95-3.432-2.541l1.443-.67c.393.997.78 1.628 1.99 1.628.789 0 1.431-.485 1.431-1.315 0-.545-.277-1.048-.947-1.514l-2.04-1.221C1.061 4.917.586 4.198.586 3.33c0-.758.27-1.393.824-1.872C1.926.993 2.569.772 3.315.772c1.443 0 2.143.756 2.78 1.929l-1.405.69c-.335-.58-.609-1.048-1.375-1.048-.379 0-.645.092-.84.25-.2.17-.305.399-.305.737 0 .214.197.567.932 1.034 1.237.78 1.952 1.243 2.082 1.351.813.688 1.25 1.524 1.25 2.493 0 .845-.285 1.558-.869 2.101M17.21 11.027V.825h1.57v8.619h2.355v1.583zM36.594 9.628c-.896 1.015-2.024 1.53-3.362 1.53-1.33 0-2.459-.517-3.361-1.53-.89-1.017-1.333-2.259-1.333-3.695 0-2.687 1.835-5.213 4.694-5.213 2.866 0 4.707 2.517 4.707 5.213 0 1.437-.445 2.678-1.345 3.695M35.51 3.311c-.601-.682-1.351-1.021-2.278-1.021-2.03 0-3.204 1.758-3.204 3.643 0 1.885 1.174 3.642 3.204 3.642.927 0 1.677-.339 2.278-1.02.612-.689.926-1.554.926-2.622 0-1.07-.314-1.934-.926-2.622"></path><path fill="#0088CE" d="M14.313 11.027l-2.68-6.79-2.667 6.79H7.325L11.633.737l4.322 10.29z"></path><path fill="#0088CE" d="M10.422 8.899l1.227 3.165L12.857 8.9zM46.838 11.226L42.385.913h1.178l4.495 10.313z"></path><path fill="#000" d="M75.95 28.903c0 .406-.284.732-.771.732a1.08 1.08 0 0 1-.895-.447L63.469 15.973h-4.716V28.66a.816.816 0 0 1-.813.813.79.79 0 0 1-.813-.813V1.133c0-.487.324-.812.813-.812a.79.79 0 0 1 .813.812V14.47h4.757L73.958.686c.204-.284.489-.446.814-.446.407 0 .731.284.731.69 0 .164-.08.407-.284.692L64.892 15.16l10.855 13.255c.123.161.204.324.204.487M79.204 27.724c-.326-.163-.57-.367-.57-.773 0-.448.285-.731.691-.731.122 0 .285.04.407.12a17.73 17.73 0 0 0 8.294 2.034c4.757 0 8.213-2.033 8.213-5.815 0-9.757-17.361-4.268-17.361-15.327 0-4.31 3.212-7.197 9.067-7.197 5.082 0 7.685 2.197 8.66 4.31.284.57.407 1.14.407 1.424 0 .448-.326.691-.691.691-.326 0-.652-.123-.854-.57-.814-2.398-2.684-4.471-7.44-4.471-4.961 0-7.563 2.399-7.563 5.691 0 9.799 17.442 4.229 17.442 15.329 0 4.35-3.334 7.358-9.96 7.358-4.026 0-7.156-1.097-8.742-2.073"></path></g></svg></div>'
        },
        "footer": {
            "height": "28mm",
            "contents": {
                default: '<div style="display: flex; justify-content: space-between"><span style="display: block; width: 100%;">Copyright &copy; SALTO KS - 2020. <a href="https://saltoks.com" >www.saltoks.com</a></div>'
            }
        }
    }
    // const title = 'Test pdf title';
    const html = fs.readFileSync(PDF_TEMPLATE, `utf-8`);

    try { 
        await fs.promises.mkdir(`${__dirname}/${EXPORT_DIR}/${folder}`, { recursive: true })
    } catch(e){
        console.log(`Failed to ensure directory ${folder}: ${e}`)
    }

    // console.log(html);
    const pdfDocument = {
        html: html,
        data: {
            title: title,
            content: content
        },
        path: `${__dirname}/${EXPORT_DIR}/${folder}/${title.replace(/[^a-z0-9]/gi, ` `)}.pdf` // We need to sanitize the title to use it as filename. Punctuation can throw errors.
    };

    console.log(`Exporting ${title}`);

    pdf.create(pdfDocument, pdfOptions).then(res => {
        console.log(`Exported ${title} in ${folder}`)
        return true;
    }).catch(e => {
        console.log(`Error exporting ${title}: ${e}`);
        return false;
    });
}
