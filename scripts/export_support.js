const // Story block constants
    StoryblokClient = require(`storyblok-js-client`),
    accessToken = `rTEXKYwPJo5RgIS25Vl9qwtt`,
    Storyblok = new StoryblokClient({
        accessToken,
    }),
    fs = require(`fs`);
let lastPage = 1;
let result = [];

const SB = {
    getAnswers: page => {
        const perPage = 100;
        console.log(`Getting next 100 stories, page: ${page}`);
        return new Promise((resolve, reject) => {
            Storyblok.get(`cdn/stories`, {
                contain_component: `lead_database_item`,
                filter_query: {
                    component: {
                        in: `lead_database_item`,
                    },
                },
                page: page,
                perPage: perPage,
            })
                .then(async res => {
                    console.log(`Successfully fetched page ${page}`);
                    let stories = res.data.stories;
                    result.push(...stories);
                    lastPage = Math.ceil(res.total / res.perPage);
                    if (page <= lastPage) {
                        page++;
                        await SB.getAnswers(page);
                    }
                    resolve(result);
                })
                .catch(e => {
                    console.log(
                        `Something went wrong fetching page ${page}. Error: ${e}`
                    );
                    reject(e);
                });
        });
    },
};

const getAllAnswers = async () => {
    console.log(`Getting all answers:`);
    let result = await SB.getAnswers(1);
    return result;
};

const init = async () => {
    let allAnswers = await getAllAnswers();
    let promises = [];

    allAnswers.forEach(async item => {
        console.log(item);

        fs.writeFile(
            `./LEADS/${item.slug}.json`,
            JSON.stringify(item),
            `utf8`,
            function(err) {
                if (err) {
                    console.error(`Couldn't write files` + err);
                }
            }
        );
    });

    console.log(`Init export of all answers...`);

    // await Promise.all(promises).catch(e => {
    // 	e.forEach(error => {
    // 		console.log(`Something went wrong during the export: ${error}`);
    // 	});
    // });
};

init();
