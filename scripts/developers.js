const
	// Story block constants
	StoryblokClient = require(`storyblok-js-client`),
	accessToken = `rTEXKYwPJo5RgIS25Vl9qwtt`,
	spaceId = `72378`, // id of space where to swap content

	Storyblok = new StoryblokClient({
		accessToken
    }),
    pdf = require("pdf-creator-node"),
    fs = require('fs')
;

let lastPage = 1;
let result = [];

const getPage = (id) => {
	console.log(id);
	return new Promise((resolve, reject) => {
		Storyblok.get(`cdn/stories/${id}`, {})
			.then(async (res) => {
				console.log(`Successfully fetched page ${id}`);
				let story = res.data.story;
				result.push(story);
				resolve(result);
			})
			.catch(e => {
				console.log(`Something went wrong fetching page ${id}. Error: ${e}`)
				reject(e)
			});
	});
}

const init = async () => {

	let stories = [`13191247`, `13938839`];

    // let allAnswers = await getAllAnswers();

	let promises = [];
	stories.map(story => {
		promises.push(getPage(story));
	});

	await Promise.all(promises);

	exportAsPDF(result);

	console.log(result);

    result.forEach(async answer => {
        let content;
        try {   
            if(answer.content.content.type) {
                content = Storyblok.richTextResolver.render(answer.content.content);
            }
        } catch(e){
            console.log(`Could parse content for ${answer.name}: ${e}`)
        }
        const 
            title = answer.name,
            folder = answer.content.category
        ;
        
        if(content){
            promises.push(exportAsPDF(title, content).catch(e => console.log(e)));
        }
    });
    
    console.log(`Init export of all answers...`);

    await Promise.all(promises).then(res => {
        console.log(`Finished exporting all answers:`);
    }).catch(e => {
        e.forEach(error => {
            console.log(`Something went wrong during the export: ${error}`);
        });
    });
    


    
}

init();

const exportAsPDF = async (title, content) => {
	console.log(title);
    const EXPORT_DIR = `DEVELOPERS`;
    const PDF_TEMPLATE = './scripts/answer_template.html'
    const pdfOptions = {
        format: "A4",
        orientation: "portrait",
        border: "10mm",
        header: {
            height: "45mm",
        }
    }
    // const title = 'Test pdf title';
    const html = fs.readFileSync(PDF_TEMPLATE, `utf-8`);

    try { 
        await fs.promises.mkdir(`${__dirname}/${EXPORT_DIR}`, { recursive: true })
    } catch(e){
        console.log(`Failed to ensure directory ${e}`)
    }

    // console.log(html);
    const pdfDocument = {
        html: html,
        data: {
            title: title,
            content: content
        },
        path: `${__dirname}/${EXPORT_DIR}/${title.replace(/[^a-z0-9]/gi, ` `)}.pdf` // We need to sanitize the title to use it as filename. Punctuation can throw errors.
    };

    console.log(`Exporting ${title}`);

    pdf.create(pdfDocument, pdfOptions).then(res => {
        console.log(`Exported ${title}`)
        return true;
    }).catch(e => {
        console.log(`Error exporting ${title}: ${e}`);
        return false;
    });
}
