import React from 'react';
import PropTypes from 'prop-types';
import SbEditable from 'storyblok-react';

const SmallTitle = (props) => {
	const fullClass = `${props.blok.classes} ${props.blok.color}`;
	return (
		<SbEditable content={props.blok}>
			<h4 className={fullClass}>{props.blok.small_title}</h4>
		</SbEditable>
	);
};

export default SmallTitle;

SmallTitle.propTypes = {
	blok: PropTypes.shape({
		classes: PropTypes.string,
		small_title: PropTypes.string.isRequired,
		color: PropTypes.string
	}).isRequired
};