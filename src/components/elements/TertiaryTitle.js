import React from 'react';
import PropTypes from 'prop-types';
import SbEditable from 'storyblok-react';

const SecondaryTitle = (props) => {
	const fullClass = `tertiary-title no-decoration ${props.blok.classes} ${props.blok.color}`;
	return (
		<SbEditable content={props.blok}>
			<h3 className={fullClass}>{props.blok.text}</h3>
		</SbEditable>
	);
};

export default SecondaryTitle;

SecondaryTitle.propTypes = {
	blok: PropTypes.shape({
		classes: PropTypes.string,
		text: PropTypes.string.isRequired,
		color: PropTypes.string
	}).isRequired
};