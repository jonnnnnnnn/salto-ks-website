import React from 'react';
import Link from 'gatsby-link';
import SbEditable from 'storyblok-react';

const Teaser = (props) => (
	<SbEditable content={props.blok}>
		<h1>Test</h1>
	</SbEditable>
);

export default Teaser;