import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { LANGUAGES } from '../../utilities/languages';
import { prepareUrl } from '../../utilities/helpers';
import { Location } from '@reach/router';

const LangSwitcher = ({ siblings }) => {
	const siblingKeys = Object.keys(siblings);
	const [showMenu, setShowMenu] = useState(false);

	return (
		<Location>
			{locProps => {
				let currentPath = locProps.location.pathname;
				if (currentPath !== `/`) {
					currentPath = currentPath.substring(1, currentPath.length);
					if (currentPath.slice(-1) === `/`) {
						currentPath = currentPath.substring(0, currentPath.length - 1);
					}
				}

				let current = {};

				siblingKeys.length ? siblingKeys.forEach(key => {
					let slug = siblings[key].full_slug || `/`;
					if(slug.length > 1 && slug.charAt(slug.length -1) === `/`){
						slug = slug.substring(0, slug.length -1);
					}
					if (slug === currentPath) {
						current = siblings[key];
					}
				}) : current = { lang: `en` };

				return (
					<div className='language-menu'>
						<div className='menu-language-selector-menu'>
							<div className="pll-parent-menu-item">
								<button onClick={() => setShowMenu(!showMenu)}>
									{LANGUAGES[current.lang] ? LANGUAGES[current.lang].name : current.lang}
								</button>
							</div>
							{(showMenu && siblingKeys.length) ?
								<ul className="sub-menu open">
									{siblings && siblingKeys.map((key, i) => {
										const item = siblings[key];
										return (
											<li className="lang-item" key={i}>
												<a
													href={prepareUrl(item.full_slug)}
												>
													{LANGUAGES[item.lang] ? LANGUAGES[item.lang].name : item.lang}
												</a>
											</li>
										);
									})}
								</ul> : null
							}
						</div>
					</div>
				);
			}

			}
		</Location>

	);
};

LangSwitcher.propTypes = {
	siblings: PropTypes.object.isRequired
};

export default LangSwitcher;
