import React from 'react';
import PropTypes from 'prop-types';
import RichText from '../elements/RichText';
import { Link } from 'gatsby';
import { FacebookShareButton, FacebookIcon, LinkedinShareButton, LinkedinIcon } from 'react-share';
import Layout from './Layout';


const Single = (props) => {
	const blok = JSON.parse(props.pageContext.blok);
	// const date = new Date(props.metadata.published).toDateString();

	const meta = {
		headline: blok.title,
		lang: `en`,
		title: `${blok.title} - SALTO KS`,
		description: blok.excerpt || `SALTO KS, SALTO Keys as a Service - Wireless Access Control For Your Business. Cloud-Based, Real-Time And On-The-Go Smart Lock Solution. Go Keyless Now!`,
		keywords: blok.keywords || `Access control, locks, wireless, cloud-based, keyless, entry, real-time`,
		schemaImg: blok.thumb[0].image,
		amp: true
	};
	return (
		<Layout
			headerStyle='transparent'
			full={true}
			meta={meta}
		>
			<div className="site-content">
				<div className="post-detail-intro row mb-4">
					<div className="post-detail-intro-image overlay w-100">
						{
							blok.hero && <img src={blok.hero[0].image} />
						}
					</div>

					<div className="post-detail-body-content position-relative container mb-5">
						<div className="row">
							<div className="col-12 post-controls position-lg-absolute d-flex justify-content-between">
								<div className="content-back-button">
									<Link to="/blog-posts" className="info">
										<i className="fa fa-arrow-left"></i>Back
									</Link>
								</div>
								<div className="col-lg-1 justify-content-lg-start flex-lg-column content-share-buttons col-7 d-flex justify-content-end text-right initial">
									<FacebookShareButton
										// url={`https://saltoks.com/${props.slug}`}
									>
										<FacebookIcon size={35} round />
									</FacebookShareButton>
									<LinkedinShareButton
										// url={`https://saltoks.com/${props.slug}`}
									>
										<LinkedinIcon size={35} round />
									</LinkedinShareButton>
								</div>
							</div>

							<div className="content-body col-12 mx-auto mt-5 col-lg-6 mt-lg-0">
								<p className="xsmall category-label">
									{/* Posted on {date} */}
								</p>

								<h2 className="text-black mb-5">
									{blok.title}
								</h2>
								<RichText amp={true} blok={blok} />
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</Layout>
	);
};

Single.propTypes = {
	slug: PropTypes.string,
	siblings: PropTypes.object,
	metadata: PropTypes.shape({
		published: PropTypes.string
	}),
	blok: PropTypes.shape({
		header_style: PropTypes.string,
		content: PropTypes.object,
		title: PropTypes.string,
		_uid: PropTypes.string.isRequired,
		hero: PropTypes.array
	}).isRequired,
	nav: PropTypes.object
};

export default Single;